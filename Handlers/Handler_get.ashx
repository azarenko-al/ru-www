﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;

using System.Net;
//using System.Collections.Specialized;

using DataSetTableAdapters;


public class Handler : IHttpHandler {

    public void ProcessRequest (HttpContext context)
    {

        //using (var client = new WebClient())
        //{
        //    var values = new NameValueCollection();
        //    values["thing1"] = "hello";
        //    values["thing2"] = "world";

        //    var response = client.UploadValues("http://www.example.com/recepticle.aspx", values);

        //    var responseString = Encoding.Default.GetString(response);
        //}


        string s;

        using (var oClient = new WebClient())
        {
             s = oClient.DownloadString("http://localhost/json/gid.json");
        }

        //        var responseString = await client.GetStringAsync("http://www.example.com/recepticle.aspx");

        //var s = new sp_billboard_SELTableAdapter().Fill(111);

        context.Response.ContentType = "application/json";
        context.Response.Write(s);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}