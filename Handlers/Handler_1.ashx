﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;

public class Handler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) 
    {

context.Response.Write("<html><body><form>");

        context.Response.Write(
          "<h2>Hello there. What's cool about .NET?</h2>");
        context.Response.Write("<select name='Feature'>");
        context.Response.Write("<option> Strong typing</option>");
        context.Response.Write("<option> Managed code</option>");
        context.Response.Write("<option> Language agnosticism</option>");
        context.Response.Write("<option> Better security model</option>");
        context.Response.Write(
           "<option> Threading and async delegates</option>");
        context.Response.Write("<option> XCOPY deployment</option>");
        context.Response.Write(
           "<option> Reasonable HTTP handling framework</option>");
        context.Response.Write("</select>");
        context.Response.Write("</br>");
       context.Response.Write(
           "<input type=submit name='Lookup' value='Lookup'></input>");
        context.Response.Write("</br>");


        if (context.Request.Params["Feature"] != null)
        {
            context.Response.Write("Hi, you picked: ");
            context.Response.Write(context.Request.Params["Feature"]);
            context.Response.Write(" as your favorite feature.</br>");
        }

        context.Response.Write("</form></body></html>");
        //context.Request.


     //   context.Response.ContentType = "text/plain";
      //  context.Response.Write("Hello World");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}